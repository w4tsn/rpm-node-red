#!/bin/sh
version=1.2.2
rm -f $version.tar.gz
wget https://github.com/node-red/node-red/archive/$version.tar.gz
tar zxf $version.tar.gz
## Download npm libraries
yarnpkg --ignore-optional --cwd node-red-$version/
printf "" > node-red-unbundle.list.tmp
printf "" > node-red-requires.list.tmp
printf "" > node-red-bundle.list.tmp
printf "" > node-red-provides.list.tmp
modules_list=($(yarn list --depth 0 --cwd node-red-$version/ | sed -nE "/^(├─|└─) (.*)@(.*)$/{p; d}" | sed -E "s/^(├─|└─) (.*)@(.*)$/\2@\3\n/gm" | sed -E 's/^(.*)@(.*)$/\1/g' | sed 's/\n+/ /g'))
modules_version=($(yarn list --depth 0 --cwd node-red-$version/ | sed -nE "/^(├─|└─) (.*)@(.*)$/{p; d}" | sed -E "s/^(├─|└─) (.*)@(.*)$/\2@\3\n/gm" | sed -E 's/^(.*)@(.*)$/\2/gm' | sed 's/\n+/ /g'))
modules_total=${#modules_list[@]}
echo "Checking if any module already exists in repository ($modules_total modules)..."
for index in ${!modules_list[@]}
do
  module=${modules_list[$index]}
  version=${modules_version[$index]}
  module_count=$(expr $index + 1)
  printf "($module_count/$modules_total) $module... "
  if grep -q "nodejs-$module" <<< $(dnf search -q nodejs-$module); then
    printf "found nodejs-$module!\n"
    echo $module >> node-red-unbundle.list.tmp
    echo "Requires:      npm($module)" >> node-red-requires.list.tmp
  else
    printf "not found.\n"
    echo $module >> node-red-bundle.list.tmp
    echo "Provides:      bundled($module) = $version" >> node-red-provides.list.tmp
  fi
done
mv node-red-unbundle.list.tmp node-red-unbundle.list
mv node-red-requires.list.tmp node-red-requires.list
mv node-red-bundle.list.tmp node-red-bundle.list
mv node-red-provides.list.tmp node-red-provides.list

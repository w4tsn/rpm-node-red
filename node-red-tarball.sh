#!/bin/sh
# Setup for this script:
# dnf install nodejs-yarn $(cat -unbundle.list | while read line; do echo "nodejs-$line"; done)
#
#version=$(rpm -q --specfile --qf='%{version}\n' node-red.spec | head -n1)
version=1.2.2
## Download node-red
wget https://github.com/node-red/node-red/archive/$version.tar.gz
tar zxf $version.tar.gz
cd node-red-$version
## Download npm libraries
yarnpkg --ignore-optional
## Clean out binary modules (and deps) and link to rpm packages
echo "Removing modules that we already have as packages "
for module in $(cat ../node-red-unbundle.list)
do
  echo -n "."
  rm -rf node_modules/$module
  ln -s /usr/lib/node_modules/$module node_modules/
  for line in $(find . -type d -name $module | grep node_modules/$module$)
  do
    if ! [ -L $line ] ; then
      echo -n "$(echo $line | tr -cd '/' | wc -c)"
      rm -rf $line
      ln -s /usr/lib/node_modules/$module $line
    fi
  done
done
echo
cd ..
tar cfz node-red-v$version.tar.gz node-red-$version
